(in-package :cl-user)

(setf *features* (remove "SB-THREAD" *features* :test #'string-equal))

#+sb-thread (error "Huh?")

(let ((*default-pathname-defaults* (merge-pathnames "lisppaste2/"
                                                    (user-homedir-pathname))))
  (load "README"))

(let ((*default-pathname-defaults* (merge-pathnames "cl-irc/example/"
                                                    (user-homedir-pathname))))
  (require :cliki-bot)
  (require :specbot)
  )

(clim-lookup:populate-table)

(defparameter *minion-channels* '("#clhs" "#ifdef" "#lisp" "#scheme" "#tech.coop" "#abcl" "#lisppaste-status"))
(defparameter *specbot-channels* '("#clhs" "#ifdef" "#lisp" "#emacs" "#scheme" "#macdev" "#concatenative" "#abcl" "#lisppaste-status"))

(let ((*default-pathname-defaults* (merge-pathnames "cl-irc/example/"
                                                    (user-homedir-pathname))))
  (setf cliki::*cliki-nickserv-password* "l1sp")
  (sleep 5)
  (apply #'cliki::start-cliki-bot "minion" "chat.freenode.net" *minion-channels*)
  (sleep 5)
  (apply #'specbot::start-specbot "specbot" "chat.freenode.net" *specbot-channels*)
  (sleep 5))

(load (compile-file "panacea"))

(sb-sys:enable-interrupt sb-unix:sigusr1
			 (lambda (signal info context)
			   (declare (ignore signal info context))
			   (panacea)
			   (invoke-restart (find-restart 'abort))))

(load (compile-file "usr2-backtrace"))

;;; SBCL, when compiled with :SB-LDB, tends to end up in an
;;; interactive low-level debugger when something nasty happens like
;;; heap memory is corrupted, total memory exhaustion, signal handling
;;; issues, etc.  We'd rather the process just die when that happens
;;; so that other software can restart it.  The magic incantation is:
#+sbcl
(sb-alien:alien-funcall (sb-alien:extern-alien "disable_lossage_handler"
					       (function sb-alien:void)))

(defun shut-up ()
  (lisppaste::shut-up)
  (cliki::shut-up)
  (specbot::shut-up))

(shut-up)